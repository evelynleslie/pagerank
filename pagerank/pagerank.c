#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <immintrin.h>

#include "pagerank.h"

typedef struct {
	unsigned int id;
	double* arr;
	double n;
	double* p;
	size_t nthreads;
	size_t npages;
	node* list;
} worker_args;

void* worker_multiply(void* args) {

	worker_args* wargs = (worker_args*) args;

	const unsigned int start = wargs->id * (wargs->npages/wargs->nthreads);
	const unsigned int end = wargs->id == wargs->nthreads - 1 ? (wargs->npages) : ((wargs->id + 1) * (wargs->npages/wargs->nthreads));

	node*cur = wargs->list;
	unsigned int h = 0;
	while(h < start){
		cur = cur->next;
		h++;
	}
	for (size_t j = start; j < end; j++, cur = cur->next) {
		page *j_page = cur->page;
		node* curs = wargs->list;
		wargs->p[j] = wargs->n;
		for(unsigned int i = 0; i < wargs->npages; i++, curs = curs->next){
			page *i_page = curs->page;
			if(i_page->noutlinks == 0){
				wargs->arr[j*wargs->npages+i] = wargs->n;
			}
			else{
				node* in = j_page->inlinks;
				if(in == NULL){
					wargs->arr[j*wargs->npages+i] = 0.0;
				}
				else{
					while(strcmp(in->page->name, i_page->name) != 0){
						if(in->next == NULL){
							wargs->arr[j*wargs->npages+i] = 0.0;
							break;
						}
						else{
							in = in->next;
						}
					}
					if(strcmp(in->page->name, i_page->name) == 0){
						double x = 1.0 / (double) i_page->noutlinks;
						wargs->arr[j*wargs->npages+i] = x;						
					}
				}
			}
		}
	}

	return NULL;
}

void pagerank(node* list, size_t npages, size_t nedges, size_t nthreads, double dampener) {

	
	double* arr = (double*)malloc(sizeof(double)*npages*npages);
	double* p_temp = (double*)malloc(sizeof(double)*npages);
	double n = 1.0 / (double) npages;
	double y = (1.0 - dampener) / (double) npages;
	double* p = (double*)malloc(sizeof(double)*npages);
	double e = 1.0;
	double temp = 0.0;
	
	worker_args args[nthreads];
	pthread_t thread_ids[nthreads];

	for (unsigned int i = 0; i < nthreads; i++) {
		args[i] = (worker_args) {
			.p = p,
			.list = list,
			.id = i,
			.npages = npages,
			.nthreads = nthreads,
			.n = n,
			.arr = arr,
		};
	}

	for (unsigned int i = 0; i < nthreads; i++) {
		pthread_create(thread_ids + i, NULL, worker_multiply, args + i);
	}

	for (unsigned int i = 0; i < nthreads; i++) {
		pthread_join(thread_ids[i], NULL);
	}
	
	for(unsigned int i = 0; i < npages*npages; i++){
		arr[i] = (arr[i]*dampener) + y;
	}
	
	while(e > 0.005){
		double e_temp = 0.0;
		
		for(unsigned int i = 0; i < npages*npages; i++){
			unsigned int j = i / (unsigned int) npages; //row
			unsigned int k = i - (j * (unsigned int) npages); //column
			
			
			if(k == 0){
				temp = 0.0;
			}
			
			temp += arr[i] * p[k];
		
			if(k == npages-1){
				p_temp[j] = temp;
				e_temp += pow((p_temp[j] - p[j]),2);
			}
		}
		
		e = sqrt(e_temp);
		
		for(unsigned int i = 0; i < npages; i++){
			p[i] = p_temp[i];
		}
		
	}
	
	node* crs = list;
	for(unsigned int i = 0; i < npages; i++, crs = crs->next){
		page* pg = crs->page;
		printf("%s %.8lf\n", pg->name, p_temp[i]);
	}
	
	free(arr);
	free(p);
	free(p_temp);
}

/*
######################################
### DO NOT MODIFY BELOW THIS POINT ###
######################################
*/

int main(int argc, char** argv) {

	/*
	######################################################
	### DO NOT MODIFY THE MAIN FUNCTION OR HEADER FILE ###
	######################################################
	*/

	config conf;

	init(&conf, argc, argv);

	node* list = conf.list;
	size_t npages = conf.npages;
	size_t nedges = conf.nedges;
	size_t nthreads = conf.nthreads;
	double dampener = conf.dampener;

	pagerank(list, npages, nedges, nthreads, dampener);

	release(list);

	return 0;
}